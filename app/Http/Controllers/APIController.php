<?php
namespace App\Http\Controllers;
use Facade\FlareClient\Http\Response;
use Illuminate\Http\Request;
// Add Value Model
use \App\Models\Value;

class APIController extends Controller
{
    // Add new record to database
    function post(Request $request){
        $params = $request->all();
        $params['saved_at'] = date('Y-m-d H:i:s');
        $data = \App\Models\Value::create($params);
        return response()->json($data);
    }

    function put(){

    }

    // Get item based on id, option: timeline
    function getItem(Value $value, Request $request) {
        
        if($request->input('timestamp')) {
            $datetime = date('Y-m-d H:i:s', $request->input('timestamp'));
            $value = $value->where('saved_at', '=', $datetime)->first();
        }
        return response()->json($value);
    }

    // Get all items
    function get(){
        return response()->json(Value::all());
    }


}
