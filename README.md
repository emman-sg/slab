# Secret Tech Task: API Exam Project

1. Domain:
   [http://secretlab.env-solutions.com](http://)
2. Repository:
   [https://bitbucket.org/emman-sg/slab](http://)

Issue:

1. Laravel bug on custom timestamp field.

   1. Description:Eloquent model does not follow defined timezone in Database server, database config or in .env file.
      Ref:

      [https://stackoverflow.com/questions/61477545/timestamps-fields-stored-as-per-my-local-timezone-but-retrieved-with-shift-3-ho](https://)

      [https://github.com/laravel/framework/issues/6086](https://)
