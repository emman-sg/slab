<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::controller(\App\Http\Controllers\APIController::class)->group(function(){
    Route::get('/object/get_all_records', 'get');
    Route::get('/object/{value}', 'getItem')->where(['value' => '[0-9]+']);
    Route::post('/object', 'post');
    Route::put('/object', 'put');
});
